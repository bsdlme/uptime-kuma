## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/uptime-kuma/badges/main/pipeline.svg)](https://gitlab.com/bastillebsd-templates/uptime-kuma/commits/main)

## Uptime-Kuma
Bastille Template for [Uptime Kuma](https://github.com/louislam/uptime-kuma) a self-hosted monitoring tool

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/uptime-kuma
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/uptime-kuma
```

